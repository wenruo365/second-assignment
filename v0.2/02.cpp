#include<stdafx.h>
#include<iostream>
#include<string>
using namespace std;
int main(int argc, char* argv[])
{
	FILE *fp;
	int character;  //字符
	int word;       //单词
	int sentence;   //句子
	char ch;
	while(true)
	{
		cout<<"本程序可统计导入的纯英文txt文本中的字符数，单词数，句子数。"<<endl;
		cout<<"请先输入想要实现的功能。"<<endl;
		cout<<" -c 为字符数 -w为单词数 -s为句子数"<<endl;
		char f[10];
		cin>>f;
		if(f[1]=='c')
		{
			character=0;
			cout<<"请输入文件地址"<<endl;
			string file;
			cin>>file; 
			if((fp=fopen((file).c_str(),"r"))==NULL)    
			{
			    cout<<"文件错误"<<endl;
			}
			ch=fgetc(fp);
			while(ch!=EOF)
			{
				character++;
				ch=fgetc(fp);
			}
			fclose(fp);
			cout<<"字符数："<<character<<endl;
		}
		if(f[1]=='w')
		{
			word=0;
			cout<<"请输入文件地址"<<endl;
			string file;
			cin>>file; 
			if((fp=fopen((file).c_str(),"r"))==NULL)   
			{
			    cout<<"文件错误"<<endl;
			}
			ch=fgetc(fp);
			while(ch!=EOF)
			{
				if(ch==' '||ch==','||ch=='.'||ch=='?'||ch=='!')
				{
					word++;
				}

				ch=fgetc(fp);
			}
			fclose(fp);
			cout<<"单词数："<<word<<endl;
		}
		if(f[1]=='s')
		{
			sentence=0; 
			cout<<"请输入文件地址"<<endl;
			string file;
			cin>>file;  
			if((fp=fopen((file).c_str(),"r"))==NULL)    
			{
			    cout<<"文件错误"<<endl;
			}
			ch=fgetc(fp);
			while(ch!=EOF)
			{
				if(ch=='.'||ch=='?'||ch=='!')
				{
					sentence++;
				}

				ch=fgetc(fp);
			}
			fclose(fp);
			cout<<"句子数："<<sentence<<endl;
		}
	}
	
    
	return 0;

}